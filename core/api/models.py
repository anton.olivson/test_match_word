from typing import Optional

from pydantic import BaseModel, constr


class TokenBaseSchema(BaseModel):
    id: int
    user_id: int
    token: str
    active: bool = None


class UserBaseSchema(BaseModel):
    id: int
    username: str
    password: str


class UserDataSchema(BaseModel):
    user_id: str
    token: str


class LoginSchema(BaseModel):
    username: str
    password: Optional[constr(max_length=10)]


class RegisterSchema(BaseModel):
    username: str
    password: Optional[constr(max_length=10)]
    password_confirm: Optional[constr(max_length=10)]


class ChangePasswordSchema(BaseModel):
    password: Optional[constr(max_length=10)]
    password_confirm: Optional[constr(max_length=10)]


class MatchSchema(BaseModel):
    word1: str
    word2: str


class MatchResultSchema(BaseModel):
    result: float



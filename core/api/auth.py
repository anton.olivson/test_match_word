from fastapi import APIRouter, HTTPException, Request, Depends
from pydantic import ValidationError

from core.api.exceptions import UserNotFoundError, UserIsAlreadyPresent, TokenNotFoundException
from core.api.models import LoginSchema, RegisterSchema, UserDataSchema, ChangePasswordSchema
from core.api.services import create_user_by_register_data, get_or_create_token_by_user, get_user_by_login_data, \
    deactivate_token, get_user_by_token, update_user_password

router = APIRouter()


@router.post("/login", response_model=UserDataSchema)
async def login(login_data: LoginSchema):
    try:
        user = await get_user_by_login_data(login_data)
        token = await get_or_create_token_by_user(user)
        return {"token": token, 'user_id': user.id}
    except UserNotFoundError:
        raise HTTPException(status_code=400, detail="Username or password is wrong")


@router.post('/register', response_model=UserDataSchema)
async def register(register_data: RegisterSchema,):
    try:
        user = await create_user_by_register_data(register_data)
        token = await get_or_create_token_by_user(user)
        return {"token": token, 'user_id': user.id}
    except UserIsAlreadyPresent:
        raise HTTPException(status_code=400, detail=f"Username with username {register_data.username} already present")


@router.get('/logout')
async def logout(request: Request):
    try:
        await deactivate_token(request.headers.get('Authorization', None))
        return {'message': 'ok'}
    except TokenNotFoundException:
        raise HTTPException(status_code=400, detail=f"Token not found")


@router.post('/change_password')
async def change_password(request: Request):
    request.headers.get('Authorization')
    try:
        user = await get_user_by_token(request.headers.get('Authorization'))
    except TokenNotFoundException:
        raise HTTPException(status_code=401, detail='Unauthorized')
    try:
        new_password = ChangePasswordSchema(**request)
        await update_user_password(user, new_password.password)
        return {'message': 'ok'}
    except ValidationError as ex:
        raise HTTPException(status_code=400, detail=str(ex))


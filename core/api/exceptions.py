class UserNotFoundError(BaseException):
    pass


class UserIsAlreadyPresent(BaseException):
    pass


class TokenNotFoundException(BaseException):
    pass
from uuid import uuid4

from core.api.exceptions import UserIsAlreadyPresent, UserNotFoundError, TokenNotFoundException
from core.api.models import UserBaseSchema, TokenBaseSchema
from core.api.utils import get_hashed_password
from core.db import database, token as token_table, user as user_table


async def get_or_create_token_by_user(user):
    token_line = str(uuid4())
    token_query = token_table.select().where(user.id == token_table.c.user_id).where(token_table.c.active == True)
    token_result = await database.fetch_one(token_query)
    if token_result:
        token = TokenBaseSchema(**token_result)
        token_line = token.token
    else:
        await database.execute(token_table.insert().values(user_id=user.id, token=token_line, active=True))
    return token_line


async def create_user_by_register_data(register_data):
    user_query = user_table.select().where(register_data.username == user_table.c.username)
    user = await database.fetch_one(user_query)
    if user:
        raise UserIsAlreadyPresent(f'User with username: {register_data.username} is present')
    hashed_password = get_hashed_password(register_data.password)
    user_id = await database.execute(user_table.insert().values(username=register_data.username, password=hashed_password))
    user_query = user_table.select().where(user_table.c.id == user_id)
    user = UserBaseSchema(**await database.fetch_one(user_query))
    await get_or_create_token_by_user(user)
    return user


async def get_user_by_login_data(login_data):
    hashed_password = get_hashed_password(login_data.password)
    user_query = user_table.select().where(login_data.username == user_table.c.username).where(
        user_table.c.password == hashed_password
    )
    result = await database.fetch_one(query=user_query)
    if result:
        return UserBaseSchema(**result)
    raise UserNotFoundError('')


async def get_or_create_token_by_login_data(login_data):
    hashed_password = get_hashed_password(login_data.password)
    query = user_table.insert().values(username=login_data.username, password=hashed_password)
    result = await database.execute(query=query)
    return result


async def deactivate_token(token_line):
    token_line = str(token_line)
    if 'Bearer' not in token_line:
        raise TokenNotFoundException()
    token_line = token_line.replace('Bearer ', '')
    query = token_table.select().where(token_line == token_table.c.token).where(token_table.c.active == True)
    token = await database.fetch_one(query)
    if token:
        query = token_table.update().where(token_table.c.id == token['id']).values(active=False)
        await database.execute(query)
    else:
        raise TokenNotFoundException()


async def get_user_by_token(token_line):
    token_line = str(token_line)
    if 'Bearer' not in token_line:
        raise TokenNotFoundException()
    token_line = token_line.replace('Bearer ', '')
    query = token_table.select().where(token_table.c.token == token_line).where(token_table.c.active == True)
    token = await database.fetch_one(query)
    if token:
        token = TokenBaseSchema(**token)
        user_query = user_table.select().where(user_table.c.id == token.user_id)
        return UserBaseSchema(**await database.fetch_one(user_query))
    else:
        raise TokenNotFoundException()


async def update_user_password(user, password):
    hashed_password = get_hashed_password(password)
    user_table.update()
    query = user_table.update().where(user_table.c.id == user.id).values(password=hashed_password)
    await database.execute(query)

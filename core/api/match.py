from fuzzywuzzy import fuzz
from fastapi import APIRouter, HTTPException, Request
from core.api.exceptions import TokenNotFoundException
from core.api.models import MatchSchema, MatchResultSchema
from core.api.services import get_user_by_token

router = APIRouter()


@router.post('/match', response_model=MatchResultSchema)
async def word_match(request: Request):
    try:
        await get_user_by_token(request.headers.get('Authorization', None))
    except TokenNotFoundException:
        raise HTTPException(status_code=401, detail='Unauthorized')
    match_obj = MatchSchema(**await request.json())
    result = fuzz.partial_token_set_ratio(match_obj.word1, match_obj.word2) or 1
    result /= 100
    return {'result': result}

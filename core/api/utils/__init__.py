import os
from hashlib import sha256


def get_hashed_password(password):
    key = os.getenv('SECRET_KEY')
    bytes_password_line = f'{key}T{password}'.encode()
    return sha256(bytes_password_line).hexdigest()

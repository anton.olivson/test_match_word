import os

from sqlalchemy import (
    Column, Integer, Boolean, MetaData,
    String, Table, create_engine, ForeignKey
)

from databases import Database

DATABASE_URL = os.getenv("DATABASE_URL")

# SQLAlchemy
engine = create_engine(DATABASE_URL)
metadata = MetaData()
user = Table(
    "user",
    metadata,
    Column("id", Integer, primary_key=True),
    Column("username", String(50), unique=True),
    Column("password", String(64)),
)

token = Table(
    'token',
    metadata,
    Column('id', Integer, primary_key=True),
    Column('user_id', Integer, ForeignKey("user.id"), nullable=False),
    Column('token', String(50)),
    Column('active', Boolean, default=True)
)

# databases query builder
database = Database(DATABASE_URL)

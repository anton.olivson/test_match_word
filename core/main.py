from fastapi import FastAPI, Request
from fastapi.responses import HTMLResponse
from starlette.templating import Jinja2Templates

from core.api import auth, match
from core.db import engine, database, metadata

metadata.create_all(engine)

templates = Jinja2Templates(directory="templates")


app = FastAPI()


@app.on_event("startup")
async def startup():
    await database.connect()


@app.on_event("shutdown")
async def shutdown():
    await database.disconnect()


app.include_router(auth.router, prefix="/auth", tags=["auth"])
app.include_router(match.router, tags=["match"])


@app.get('/', response_class=HTMLResponse)
async def index(request: Request):
    return templates.TemplateResponse("index.html", context={'request': request})
